from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.keys import Keys #키에 관련된 모듈 가져오기
import time
import sys #파일로 저장하기 위한 모듈
import os #폴더를 다루기 위한 모듈

import urllib.request
import urllib
import re #정규표현식 관련 '레귤러 익스프레션스'
import math #수학
import random #랜덤
import pandas as pd # dataframe



def image_save(imgUrl, imgNm, idx):
    try:
        urllib.request.urlretrieve(imgUrl,imgNm)
        print(f"{idx} 번째 저장완료")    
    except :
        print(f"{idx} 번째 이미지 없음")    
        pass


df = pd.read_excel('C:/classification/data/dispGood.xlsx', engine='openpyxl')

# Index(['good_no', 'display_good_nm', 'ctg_depth_1_no', 'ctg_depth_1_nm',
#        'ctg_depth_2_no', 'ctg_depth_2_nm', 'ctg_depth_3_no', 'ctg_depth_3_nm',
#        'sex_cd', 'img_url'],
#       dtype='object')

#print(df.columns) # 컬럼
#print(df['depth1_img_nm'][0]) # 컬럼


imgUrlLs = df['img_url']
imgNmLs = df['img_nm']

# for idx in range(1):
for idx in range(len(imgUrlLs)):
    imgUrl = imgUrlLs[idx]
    imgNm = imgNmLs[idx]

    image_save(imgUrl,'C:/classification/1depth/' + str(imgNm), idx)


